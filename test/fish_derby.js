const FishDerby = artifacts.require("./FishDerby.sol");

contract("FishDerby", accounts => {
  it("setTournamentTitle('New Derby') returned the string 'New Derby'", async () => {
    const newFishDerby = await FishDerby.deployed();

    // Set tournament_title to 'New Derby'
    await newFishDerby.setTournamentTitle("New Derby", { from: accounts[0] });

    // Get tournament_title from public variable getter
    const storedString = await newFishDerby.tournament_title.call();

    assert.equal(storedString, "New Derby", "The string was not stored");
  });

  it("addCompetitor(" + accounts[1] + ", \"Brad\", \"Kreager\") -> getCompetitor(" + accounts[1] + ") returned correctly", async () => {
    const newFishDerby = await FishDerby.deployed();

    // Set tournament_title to 'New Derby'
    await newFishDerby.addCompetitor( accounts[1], "Brad", "Kreager", { from: accounts[1] });

    // Get tournament_title from public variable getter
    const storedString = await newFishDerby.getCompetitor.call(accounts[1]);

    assert.equal(storedString[0], "Brad", "First Name was not \'Brad\'");
    assert.equal(storedString[1], "Kreager", "Last Name was not \'Kreager\'");
  });

  it("addFish( " + accounts[1] + ", \"Bass\", \"\", 24, 9) -> getFish(0) returned correctly", async () => {
    const newFishDerby = await FishDerby.deployed();

    await newFishDerby.addFish( accounts[1], "Bass", "", 24, 9, { from: accounts[1] });

    const storedString = await newFishDerby.getFish(accounts[1], 0, {from: accounts[1]});

    assert.equal(storedString[0], "Bass", " fish_species was not \'Bass\'");
    assert.equal(storedString[1], "", "fish_picture was not \'\'");
    assert.equal(storedString[2], "24", "fish_length was not \'24\'");
    assert.equal(storedString[3], "9", "fish_weight was not \'9\'");
  });

  it("test checkCompetitorIsEnrolled()", async () => {
    const newFishDerby = await FishDerby.deployed();

    await newFishDerby.addCompetitor( accounts[2], "Big", "Loser", { from: accounts[2] });

    const res = await newFishDerby.checkCompetitorIsEnrolled( accounts[2], { from: accounts[2] });

    assert.equal(res, true, "The competitor account, \'" + accounts[2] + "\' is not enrolled, test failed");
  });

  it("test setWinningPercentages()", async () => {
    const newFishDerby = await FishDerby.deployed();

    await newFishDerby.setWinningsPercentages(60, 20, 20, { from: accounts[0]})

    res = await newFishDerby.disperse_winnings_percentages(0, { from: accounts[0] });

    assert.equal(res, 60, "first_place_payout was not changed from 40 to 60");
  });
});

contract("FishDerby", accounts => {
  it("test setFirstPlace()", async () => {
    const newFishDerby = await FishDerby.deployed();

    await newFishDerby.addCompetitor(accounts[1], "Brad", "Kreager", { from: accounts[1] });
    await newFishDerby.addFish(accounts[1], "Bass", "", 24, 9, { from: accounts[1] });

    await newFishDerby.addCompetitor(accounts[2], "comp", "2", { from: accounts[2] });
    await newFishDerby.addFish(accounts[2], "Bass", "", 22, 8, { from: accounts[2] });

    await newFishDerby.addCompetitor(accounts[3], "comp", "3", { from: accounts[3] });
    await newFishDerby.addFish(accounts[3], "Bass", "", 24, 7, { from: accounts[3] });

    await newFishDerby.addCompetitor(accounts[4], "comp", "4", { from: accounts[4] });
    await newFishDerby.addFish(accounts[4], "Bass", "", 20, 6, { from: accounts[4] });

    await newFishDerby.addCompetitor(accounts[5], "comp", "5", { from: accounts[5] });
    await newFishDerby.addFish(accounts[5], "Bass", "", 20, 5, { from: accounts[5] });

    await newFishDerby.addCompetitor(accounts[6], "comp", "6", { from: accounts[6] });
    await newFishDerby.addFish(accounts[6], "Bass", "", 20, 4, { from: accounts[6] });

    await newFishDerby.setWinners({ from: accounts[0] });

    first_place_before = await newFishDerby.first_place({ from: accounts[0]});
    console.log("    first place (before): " + first_place_before);

    second_place = await newFishDerby.second_place({ from: accounts[0]});
    console.log("    second place: (before)" + second_place);

    await newFishDerby.setFirstPlace(accounts[2], { from: accounts[0] });

    first_place = await newFishDerby.first_place({ from: accounts[0]});
    console.log("    first place: " + first_place);

    second_place = await newFishDerby.second_place({ from: accounts[0]});
    console.log("    second place: " + second_place);

    assert.equal(first_place_before, second_place, "First Place was not switched to second place");
  });
});

contract("FishDerby", accounts => {
  it("test endTournament()", async () => {
    const newFishDerby = await FishDerby.deployed();

    await newFishDerby.addCompetitor(accounts[1], "comp", "1", { from: accounts[1] });
    await newFishDerby.addFish(accounts[1], "Bass", "", 22, 9, { from: accounts[1] });

    await newFishDerby.addCompetitor(accounts[2], "comp", "2", { from: accounts[2] });
    await newFishDerby.addFish(accounts[2], "Bass", "", 22, 8, { from: accounts[2] });

    await newFishDerby.addCompetitor(accounts[3], "comp", "3", { from: accounts[3] });
    await newFishDerby.addFish(accounts[3], "Bass", "", 24, 7, { from: accounts[3] });

    await newFishDerby.addCompetitor(accounts[4], "comp", "4", { from: accounts[4] });
    await newFishDerby.addFish(accounts[4], "Bass", "", 20, 6, { from: accounts[4] });

    await newFishDerby.addCompetitor(accounts[5], "comp", "5", { from: accounts[5] });
    await newFishDerby.addFish(accounts[5], "Bass", "", 20, 5, { from: accounts[5] });

    await newFishDerby.addCompetitor(accounts[6], "comp", "6", { from: accounts[6] });
    await newFishDerby.addFish(accounts[6], "Bass", "", 20, 4, { from: accounts[6] });

    await newFishDerby.addCompetitor(accounts[7], "comp", "7", { from: accounts[7] });
    await newFishDerby.addFish(accounts[7], "Bass", "", 20, 4, { from: accounts[7] });

    await newFishDerby.addCompetitor(accounts[8], "comp", "8", { from: accounts[8] });
    await newFishDerby.addFish(accounts[8], "Bass", "", 20, 4, { from: accounts[8] });

    await newFishDerby.addCompetitor(accounts[9], "comp", "9", { from: accounts[9] });
    await newFishDerby.addFish(accounts[9], "Bass", "", 20, 4, { from: accounts[9] });

    await newFishDerby.deposit({ from: accounts[0], value: web3.utils.toWei("1", 'Ether') });

    balance = await newFishDerby.getBalance({from: accounts[0]});

    console.log("    Tournament balance: " + balance.toString());

    first_place_balance = await web3.eth.getBalance(accounts[1]);
    second_place_balance = await web3.eth.getBalance(accounts[2]);
    third_place_balance = await web3.eth.getBalance(accounts[3]);
    console.log("    account[1] initial balance: " + first_place_balance);
    console.log("    account[2] initial balance: " + second_place_balance);
    console.log("    account[3] initial balance: " + third_place_balance);

    await newFishDerby.setWinningsPercentages(40, 40, 20 , { from: accounts[0] })
    const gas = await newFishDerby.setWinners.estimateGas({ from: accounts[0] });
    await newFishDerby.setWinners({ from: accounts[0] });
    await newFishDerby.endTournament({from: accounts[0]});
    balance = await newFishDerby.getBalance({from: accounts[0]});
    first_place_balance_final = (await web3.eth.getBalance(accounts[1]) - first_place_balance);
    second_place_balance_final = (await web3.eth.getBalance(accounts[2]) - second_place_balance);
    third_place_balance_final = (await web3.eth.getBalance(accounts[3]) - third_place_balance);
    console.log("    Tournament Balance: " + balance.toString());
    console.log("    account[1] balance increase: " + first_place_balance_final);
    console.log("    account[2] balance increase: " + second_place_balance_final);
    console.log("    account[3] balance increase: " + third_place_balance_final);
    assert.equal(first_place_balance_final, 400000000000000000, "First Place was not switched to second place");
    assert.equal(second_place_balance_final, 400000000000000000, "First Place was not switched to second place");
    assert.equal(third_place_balance_final, 200000000000000000, "First Place was not switched to second place");

  });
});

contract("FishDerby", accounts => {
  it("test gas cost of setWinners()", async () => {
    const newFishDerby = await FishDerby.deployed();

    try {
      await newFishDerby.addCompetitor(accounts[1], "Brad", "Kreager", { from: accounts[1] });
      await newFishDerby.addFish(accounts[1], "Bass", "", 24, 8, { from: accounts[1] });

      var gas = await newFishDerby.setWinners.estimateGas({ from: accounts[0] });
      console.log("    estimateGas return:" + gas);

      await newFishDerby.addCompetitor(accounts[2], "comp", "2", { from: accounts[2] });
      await newFishDerby.addFish(accounts[2], "Bass", "", 22, 6, { from: accounts[2] });

      gas = await newFishDerby.setWinners.estimateGas({ from: accounts[0] });
      console.log("    estimateGas return:" + gas);

      await newFishDerby.addCompetitor(accounts[3], "comp", "3", { from: accounts[3] });
      await newFishDerby.addFish(accounts[3], "Bass", "", 24, 8, { from: accounts[3] });

      gas = await newFishDerby.setWinners.estimateGas({ from: accounts[0] });
      console.log("    estimateGas return:" + gas);

      await newFishDerby.addCompetitor(accounts[4], "comp", "4", { from: accounts[4] });
      await newFishDerby.addFish(accounts[4], "Bass", "", 20, 7, { from: accounts[4] });

      gas = await newFishDerby.setWinners.estimateGas({ from: accounts[0] });
      console.log("    estimateGas return:" + gas);

      await newFishDerby.addCompetitor(accounts[5], "comp", "5", { from: accounts[5] });
      await newFishDerby.addFish(accounts[5], "Bass", "", 20, 7, { from: accounts[5] });

      gas = await newFishDerby.setWinners.estimateGas({ from: accounts[0] });
      console.log("    estimateGas return:" + gas);

      await newFishDerby.addCompetitor(accounts[6], "comp", "6", { from: accounts[6] });
      await newFishDerby.addFish(accounts[6], "Bass", "", 20, 7, { from: accounts[6] });

      gas = await newFishDerby.setWinners.estimateGas({ from: accounts[0] });
      console.log("    estimateGas return:" + gas);

      await newFishDerby.addCompetitor(accounts[7], "comp", "7", { from: accounts[7] });
      await newFishDerby.addFish(accounts[7], "Bass", "", 20, 4, { from: accounts[7] });

      gas = await newFishDerby.setWinners.estimateGas({ from: accounts[0] });
      console.log("    estimateGas return:" + gas);

      await newFishDerby.addCompetitor(accounts[8], "comp", "8", { from: accounts[8] });
      await newFishDerby.addFish(accounts[8], "Bass", "", 20, 4, { from: accounts[8] });

      gas = await newFishDerby.setWinners.estimateGas({ from: accounts[0] });
      console.log("    estimateGas return:" + gas);

      await newFishDerby.addCompetitor(accounts[9], "comp", "9", { from: accounts[9] });
      await newFishDerby.addFish(accounts[9], "Bass", "", 20, 4, { from: accounts[9] });

      gas = await newFishDerby.setWinners.estimateGas({ from: accounts[0] });
      console.log("    estimateGas return:" + gas);
    }
    catch (err) {
      console.log("    estimateGas failed");
    }
  });
});

const fs = require('fs');

contract("FishDerby", accounts => {
  it("testing image size to gas cost", async () => {
    const newFishDerby = await FishDerby.deployed();

    await newFishDerby.addCompetitor(accounts[1], "Brad", "Kreager", { from: accounts[1] });

    for (var i = 1; i < 11; i++) {
      try {
        const imgbuff = fs.readFileSync("./test/data/" + i);
        const imgb64 = imgbuff.toString('base64');

        const gas = await newFishDerby.addFish.estimateGas(accounts[1], "Bass", imgb64, 24, 9, { from: accounts[1] });
        console.log("    " + i + "kB, estimateGas returned:" + gas);
      }
      catch (error) {
        console.log("    File Size: " + i + "kB failed");
      }
    }
  });
});

contract("FishDerby", accounts => {
  it("estimating gas costs of methods", async () => {
    const newFishDerby = await FishDerby.deployed();
    var gas;
    gas = await newFishDerby.deposit.estimateGas({ from: accounts[0], value: web3.utils.toWei("1", 'Ether') });
    console.log("    deposit() - estimateGas returned:" + gas);
    gas = await newFishDerby.getBalance.estimateGas({from: accounts[0]});
    console.log("    getBalance() - estimateGas returned:" + gas);
    gas = await newFishDerby.setTournamentTitle.estimateGas("New Derby", { from: accounts[0] });
    console.log("    setTournamentTitle() - estimateGas returned:" + gas);
    gas = await newFishDerby.addCompetitor.estimateGas(accounts[1], "Brad", "Kreager", { from: accounts[1] });
    await newFishDerby.addCompetitor(accounts[1], "Brad", "Kreager", { from: accounts[1] });
    console.log("    addCompetitor() - estimateGas returned:" + gas);
    gas = await newFishDerby.checkCompetitorIsEnrolled.estimateGas( accounts[1], { from: accounts[1] });
    console.log("    checkCompetitorIsEnrolled() - estimateGas returned:" + gas);
    gas = await newFishDerby.getRoster.estimateGas({from: accounts[0]});
    console.log("    getRoster() - estimateGas returned:" + gas);
    gas = await newFishDerby.getCompetitor.estimateGas(accounts[0]);
    console.log("    getCompetitor() - estimateGas returned:" + gas);
    gas = await newFishDerby.setFirstPlace.estimateGas(accounts[0]);
    console.log("    setFirstPlace() - estimateGas returned:" + gas);
    gas = await newFishDerby.setSecondPlace.estimateGas(accounts[0]);
    console.log("    setSecondPlace() - estimateGas returned:" + gas);
    gas = await newFishDerby.setThirdPlace.estimateGas(accounts[0]);
    console.log("    setThirdPlace() - estimateGas returned:" + gas);
    gas = await newFishDerby.getWinners.estimateGas();
    console.log("    getWinners() - estimateGas returned:" + gas);
    gas = await newFishDerby.lockContract.estimateGas();
    console.log("    lockContract() - estimateGas returned:" + gas);
    gas = await newFishDerby.unlockContract.estimateGas();
    console.log("    unlockContract() - estimateGas returned:" + gas);
    gas = await newFishDerby.lockRoster.estimateGas();
    console.log("    lockRoster() - estimateGas returned:" + gas);
    gas = await newFishDerby.unlockRoster.estimateGas();
    console.log("    unlockRoster() - estimateGas returned:" + gas);
    gas = await newFishDerby.removeFromRoster.estimateGas(accounts[1]);
    console.log("    removeFromRoster() - estimateGas returned:" + gas);
    gas = await newFishDerby.setWinningsPercentages.estimateGas(60,40,20, { from: accounts[0] });
    console.log("    setWinningsPercentages() - estimateGas returned:" + gas);
    gas = await newFishDerby.getWinningsPercentages.estimateGas();
    console.log("    getWinningsPercentages() - estimateGas returned:" + gas);
    await newFishDerby.setWinners({from: accounts[0]});
    gas = await newFishDerby.endTournament.estimateGas({from: accounts[0]});
    console.log("    endTournament() - estimateGas returned:" + gas);
  });
});