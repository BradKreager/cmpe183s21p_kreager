// SPDX-License-Identifier: GPL-3.0
pragma solidity >=0.7.0 <0.9.0;

import './fish_derby.sol';
/**
 * @title Fish Derby Factory
 * @notice This app is for creating and organizing tournaments for sport fishing.
 * 
 * @dev Register anglers, rules, time-limit, and rewards for payout 
 */
contract FishDerbyFactory {

    

    /**
     * @dev Set contract storage variables
     */
    constructor() {
    }



}
