// SPDX-License-Identifier: GPL-3.0
pragma solidity >=0.7.0 <0.9.0;

/**
 * @title Fish Derby 
 * @notice This app is for creating and organizing tournaments for sport fishing.
 * 
 * @dev Register anglers, rules, time-limit, and rewards for payout 
 */
contract FishDerby {

    struct FishStats {
        string fish_species;
        string fish_picture;
        uint32 length;
        uint32 weight;
    }
    
    struct AnglerStats {
        string first_name;
        string last_name;
        uint total_inventory_weight;
        FishStats[] fish_inventory;
    }

    bool public roster_locked;
    bool public tournament_locked;
    bool public payout_complete;
    address public contract_creator;
    string public tournament_title;
    address public first_place;
    address public second_place;
    address public third_place;
    uint public first_place_payout;
    uint public second_place_payout;
    uint public third_place_payout;
    address[] public winners;
    address[] public roster;
    uint public entry_fee;
    uint public tournament_balance;
    uint32 public end_date;
    uint32 public end_time;
    uint32 public max_fish_per_competitor = 5;
    string[] public rules_string;
    uint[3] public disperse_winnings_percentages = [40, 40, 20];

    mapping (address => AnglerStats) public competitors;

    event Received(address, uint);

    // modifier to check if caller is owner
    modifier isOwner() {
        require(msg.sender == contract_creator, "Caller is not the contract creator");
        _;
    }

    // modifier to check if the contract is locked 
    modifier isOpen() {
        require(tournament_locked == false, "The tournament has ended");
        _;
    }

    /**
     * @dev Set contract storage variables
     */
    constructor(string memory title, uint _entry_fee) {
        contract_creator = msg.sender;
        roster_locked = false;
        tournament_locked = false;
        payout_complete = false;
        tournament_title = title;
        entry_fee = _entry_fee; 
    }

    /**
     * @dev Deposit funds to tournament balance for payout (Owner only) 
     */
    function deposit() external payable isOwner isOpen {
        require(msg.value % 2 == 0, "Tournament payout must be an even number");
        tournament_balance += msg.value;
        emit Received(msg.sender, msg.value);
    }

    /**
     * @dev Destroys the contract and returns funds to the contract creator account
     */
    function destroy() external payable { 
        if (msg.sender == contract_creator) {
            selfdestruct(payable(contract_creator));
        }
    }

    /**
     * @dev Gets the contract ether balance
     */
    function getBalance() external view returns (uint) {
        // return address(this).balance;
        return tournament_balance;
    }

    /**
     * @dev Sets the tournament title
     */
    function setTournamentTitle(string memory set_title) public isOwner isOpen {
        tournament_title = set_title;
    }

    /**
     * @dev check if a competitor is listed in the roster
     */
    function checkCompetitorIsEnrolled(address account) external view returns (bool) {
        for(uint i = 0; i < roster.length; i++) {
            if(roster[i] == account) {
                return true;
            }
        }
        return false;
    }

    /**
     * @dev adds a competitor to the contract
     */
    function addCompetitor(address account, string calldata first_name, string calldata last_name) external payable isOpen {
       require(!roster_locked, "The tournament roster has been locked");
       require(msg.sender != contract_creator, "Contract creator account cannot be a competitor");
       require(msg.value == entry_fee, "The sent value was different then the entry fee");
       roster.push(account);
       competitors[account].first_name = first_name;
       competitors[account].last_name = last_name;
       competitors[account].total_inventory_weight = 0;
    }

    /**
     * @dev adds a fish to a competitor's fish_inventory 
     */
    function addFish(address account, string calldata fish_species, string calldata fish_pic, uint32 fish_length, uint32 fish_weight) public isOpen {
        require(this.checkCompetitorIsEnrolled(account), "This account is not enrolled in the competition");
        require(msg.sender == account, "Transaction sender account must be the competitors account");
        require(competitors[account].fish_inventory.length <= max_fish_per_competitor, "Catch limit has been reached");

        competitors[account].total_inventory_weight += fish_weight;

        competitors[account].fish_inventory.push(
            FishStats({
                fish_species: fish_species,
                fish_picture: fish_pic,
                length: fish_length,
                weight: fish_weight
            })
        );
    }

    /**
     * @dev ends the tournament and pays winning accounts 
     */
    function endTournament() external payable isOwner {
        require(tournament_locked == true, "The tournament has not ended, set winners to end the tournament");
        require(payout_complete == false, "The payouts have been distributed by a previous call to endTournament()");
        payable(first_place).transfer(first_place_payout);
        tournament_balance -= first_place_payout;
        payable(second_place).transfer(second_place_payout);
        tournament_balance -= second_place_payout;
        payable(third_place).transfer(third_place_payout);
        tournament_balance -= third_place_payout;
        payout_complete = true;
    }

    /**
     * @dev Return a fish in a competitor's inventory 
     */
    function setMaxFishPerComp(uint32 max_fish_limit) public isOpen {
        max_fish_per_competitor = max_fish_limit;
    }

    /**
     * @dev Return a fish in a competitor's inventory 
     * @return competitors[account].fish_inventory[fish_index]
     */
    function getFish(address account, uint fish_index) external view returns (FishStats memory) {
        return competitors[account].fish_inventory[fish_index];
    }

    /**
     * @dev Returns the roster array
     * @return roster[]
     */
    function getRoster() external view returns (address[] memory) {
        return roster;
    }

    /**
     * @dev Return a competitor structure
     * @return competitors[account].fish_inventory[fish_index]
     */
    function getCompetitor(address account) external view returns (AnglerStats memory) {
        return competitors[account];
    }

    /**
     * @dev Sorts the roster by competitors' total_inventory_weight to denote winners.
     */
    function setWinners() public isOwner isOpen {
        tournament_locked = true;

        for(uint n = 0; n < roster.length; n++) {
            winners.push(roster[n]);
        }

        for(uint n = 0; n < roster.length; n++) {
            uint largest_fish = competitors[winners[n]].total_inventory_weight;
            for(uint i = n + 1; i < roster.length; i++) {
                uint check_weight = competitors[winners[i]].total_inventory_weight;
                if(check_weight > largest_fish) {
                    address temp;
                    largest_fish = check_weight; 
                    temp = winners[n];
                    winners[n] = winners[i];
                    winners[i] = temp;
                }
            }
        }

        uint balance = address(this).balance;

        if(roster.length > 0) {
            first_place = winners[0];
            first_place_payout = (balance / 100) * (disperse_winnings_percentages[0]);
        }
        if(roster.length > 1) {
            second_place = winners[1];
            second_place_payout = (balance / 100) * (disperse_winnings_percentages[1]);
        }
        if(roster.length > 2) {
            third_place = winners[2];
            third_place_payout = (balance / 100) * (disperse_winnings_percentages[2]);
        }

    }

    /**
     * @dev Allow for manually changing the first place winner set by setWinners()
     */
    function setFirstPlace(address account) public isOwner {
        address temp = first_place;
        first_place = account;
        if(second_place == account) second_place = temp;
        else if(third_place == account) third_place = temp; 
    }

    /**
     * @dev Allow for manually changing the second place winner set by setWinners()
     */
    function setSecondPlace(address account) public isOwner {
        address temp = second_place;
        second_place = account;
        if(first_place == account) first_place = temp;
        else if(third_place == account) third_place = temp; 
    }

    /**
     * @dev Allow for manually changing the third place winner set by setWinners()
     */
    function setThirdPlace(address account) public isOwner {
        address temp = second_place;
        third_place = account;
        if(first_place == account) first_place = temp;
        else if(second_place == account) second_place = temp; 
    }

    /**
     * @dev Returns the winners array.
     */
    function getWinners() external view returns (address[] memory) {
        return winners;
    }

    /**
     * @dev lock the contract
     */
    function lockContract() public isOwner isOpen {
        tournament_locked = true;
    }

    /**
     * @dev lock the contract
     */
    function unlockContract() public isOwner {
        tournament_locked = true;
    }

    /**
     * @dev lock the roster
     */
    function lockRoster() public isOwner isOpen {
        roster_locked = true;
    }

    /**
     * @dev unlock the roster
     */
    function unlockRoster() public isOwner isOpen {
        roster_locked = false;
    }

    /**
     * @dev Removes a competitor account from the roster account
     */
    function removeFromRoster(address account) public isOpen {
        require(this.checkCompetitorIsEnrolled(account), "This account is not enrolled in the competition");
        require(msg.sender == account || msg.sender == contract_creator, "Transaction sender account must be the competitors account or contract creator account");
        uint target_index = 0;

        for(uint i = 0; i < roster.length; i++) {
            if(roster[i] == account) {
                target_index = i;
                break;
            }
        }

        for(uint i = target_index; i < (roster.length - 1); i++) {
            roster[i] = roster[i + 1];
        }

        roster.pop();
    }

    /**
     * @dev Sets the percentages for payout of the tournament balance to winners
     */
    function setWinningsPercentages(uint first_place_percentage, uint second_place_percentage, uint third_place_percentage) public isOwner {
        disperse_winnings_percentages[0] = first_place_percentage;
        disperse_winnings_percentages[1] = second_place_percentage;
        disperse_winnings_percentages[2] = third_place_percentage;
        uint balance = address(this).balance;
        first_place_payout = (balance / 100) * (first_place_percentage);
        second_place_payout = (balance / 100) * (second_place_percentage);
        third_place_payout = (balance / 100) * (third_place_percentage);
    }

    /**
     * @dev Gets the percentages for dispersal of the contract funds to winners
     */
    function getWinningsPercentages() external view returns (uint256[3] memory) {
        return disperse_winnings_percentages;
    }
}