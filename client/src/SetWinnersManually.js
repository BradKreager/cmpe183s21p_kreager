import React from "react";
import { Button } from "antd";
import GetFirstPlace from "./GetFirstPlace";
import GetSecondPlace from "./GetSecondPlace";
import GetThirdPlace from "./GetThirdPlace";

class SetWinnersManually extends React.Component {
  state = { 
      stackId_first: null,
      stackId_second: null,
      stackId_third: null
  };

  handleOnClickFirstPlace = () => {
    const { drizzle, drizzleState } = this.props;
    const contract = drizzle.contracts.FishDerby;
    const stackId_first = contract.methods.setFirstPlace.cacheSend(document.getElementById("SetFirstPlaceManually").value, {
      from: drizzleState.accounts[0]
    });

    // save the `stackId` for later reference
    this.setState({ stackId_first });
  };

  handleOnClickSecondPlace = () => {
    const { drizzle, drizzleState } = this.props;
    const contract = drizzle.contracts.FishDerby;
    const stackId_second = contract.methods.setSecondPlace.cacheSend(document.getElementById("SetSecondPlaceManually").value, {
      from: drizzleState.accounts[0]
    });

    // save the `stackId` for later reference
    this.setState({ stackId_second });
  };

  handleOnClickThirdPlace = () => {
    const { drizzle, drizzleState } = this.props;
    const contract = drizzle.contracts.FishDerby;
    const stackId_third = contract.methods.setThirdPlace.cacheSend(document.getElementById("SetThirdPlaceManually").value, {
      from: drizzleState.accounts[0]
    });

    // save the `stackId` for later reference
    this.setState({ stackId_third });
  };

  getTxStatus = (stackId) => {
    // get the transaction states from the drizzle state
    const { transactions, transactionStack } = this.props.drizzleState;

    // get the transaction hash using our saved `stackId`
    const txHash = transactionStack[stackId];

    // if transaction hash does not exist, don't display anything
    if (!txHash) return null;

    // otherwise, return the transaction status
    return `Transaction status: ${transactions[txHash] && transactions[txHash].status}`;
  };

  render() {
    return (
      <div>
        <GetFirstPlace
          drizzle={this.props.drizzle}
          drizzleState={this.props.drizzleState}
        />
        <GetSecondPlace
          drizzle={this.props.drizzle}
          drizzleState={this.props.drizzleState}
        />
        <GetThirdPlace
          drizzle={this.props.drizzle}
          drizzleState={this.props.drizzleState}
        />
        <br />
        <label for="SetFirstPlaceManually">First Place Account: </label>
        <input type="text" id="SetFirstPlaceManually" /><br />
        <br />
        <Button onClick={this.handleOnClickFirstPlace}>Submit</Button>
        <div>{this.getTxStatus(this.state.stackId_first)}</div>
        <label for="SetSecondPlaceManually">Second Place Account: </label>
        <input type="text" id="SetSecondPlaceManually" /><br />
        <br />
        <Button onClick={this.handleOnClickSecondPlace}>Submit</Button>
        <div>{this.getTxStatus(this.state.stackId_second)}</div>
        <label for="SetThirdPlaceManually">Third Place Account: </label>
        <input type="text" id="SetThirdPlaceManually" /><br />
        <br />
        <Button onClick={this.handleOnClickThirdPlace}>Submit</Button>
        <div>{this.getTxStatus(this.state.stackId_third)}</div>
      </div>
    );
  }
}

export default SetWinnersManually;