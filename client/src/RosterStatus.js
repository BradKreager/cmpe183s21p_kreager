import React from "react";
import { Typography, Button } from "antd";

class RosterStatus extends React.Component {
    state = {
        dataKey: null,
        stackIdLock: null,
        stackIdUnlock: null
    };

    async getContractCreator() {
        const { drizzle } = this.props;
        const contract = drizzle.contracts.FishDerby;
        const dataKey = contract.methods.roster_locked.cacheCall();
        this.setState({ dataKey });
    };

    componentDidMount() {
        this.getContractCreator();
    }

    handleLockOnClick = () => {
        const { drizzle, drizzleState } = this.props;
        const contract = drizzle.contracts.FishDerby;
        const stackIdLock = contract.methods.lockRoster.cacheSend({
            from: drizzleState.accounts[0]
        });
        this.setState({ stackIdLock });
    };

    handleUnlockOnClick = () => {
        const { drizzle, drizzleState } = this.props;
        const contract = drizzle.contracts.FishDerby;
        const stackIdUnlock = contract.methods.unlockRoster.cacheSend({
            from: drizzleState.accounts[0]
        });
        this.setState({ stackIdUnlock });
    };

    getTxStatus = () => {
        // get the transaction states from the drizzle state
        const { transactions, transactionStack } = this.props.drizzleState;

        // get the transaction hash using our saved `stackId`
        const txHash = transactionStack[this.state.stackId];

        // if transaction hash does not exist, don't display anything
        if (!txHash) return null;

        // otherwise, return the transaction status
        return `Transaction status: ${transactions[txHash] && transactions[txHash].status}`;
    };

    render() {
        const { FishDerby } = this.props.drizzleState.contracts;
        const { drizzleState } = this.props;
        const roster_status_bool = FishDerby.roster_locked[this.state.dataKey];
        if (typeof (roster_status_bool) === 'object' && roster_status_bool.value === true) {
            return (
                <div>
                    <p>Status: Locked</p>
                    <Button onClick={this.handleUnlockOnClick}>Unlock Roster</Button>
                    <Button onClick={this.handleLockOnClick}>Lock Roster</Button>
                </div>
            );
        }
        else if (typeof (roster_status_bool) === 'object' && roster_status_bool.value === false) {
            return (
                <div>
                    <p>Status: Unlocked</p>
                    <Button onClick={this.handleUnlockOnClick}>Unlock Roster</Button>
                    <Button onClick={this.handleLockOnClick}>Lock Roster</Button>
                </div>
            );
        }
        else {
            return null;
        }
    }
}

export default RosterStatus;