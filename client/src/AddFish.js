import React from "react";
import { Button } from "antd";

class AddFish extends React.Component {
  state = { 
    stackId: null, 
    base64String: ""
  };

  handleOnClick = () => {
    this.setValue(
      // document.getElementById("AddFishAccount").value,
      document.getElementById("AddFishSpecies").value,
      document.getElementById("AddFishPic").innerHTML,
      document.getElementById("AddFishLength").value,
      document.getElementById("AddFishWeight").value
    );
  };

  setValue = (fish_species, fish_pic, fish_length, fish_weight)=> {
    const { drizzle, drizzleState } = this.props;
    const contract = drizzle.contracts.FishDerby;
    // contract.methods.addFish(drizzleState.accounts[0], fish_species, fish_pic, fish_length, fish_weight).estimateGas({
    //   from: drizzleState.accounts[0],
    //   gas: 6721975
    // })
    // .then((error, gasAmount) => {
    //   if(gasAmount == 6721975) {
    //     console.log("ran out of gas");
    //   }
    //   if(error) {
    //     console.log(error);
    //   }
    // });

    const stackId = contract.methods.addFish.cacheSend(drizzleState.accounts[0], fish_species, fish_pic, fish_length, fish_weight, {
      from: drizzleState.accounts[0]
    });
    this.setState({ stackId });
  };

  getTxStatus = () => {
    // get the transaction states from the drizzle state
    const { transactions, transactionStack } = this.props.drizzleState;

    // get the transaction hash using our saved `stackId`
    const txHash = transactionStack[this.state.stackId];

    // if transaction hash does not exist, don't display anything
    if (!txHash) return null;

    // otherwise, return the transaction status
    return `Transaction status: ${transactions[txHash] && transactions[txHash].status}`;
  };

  imageUploaded = event => {
    var file = event.target.files[0];
    var reader = new FileReader();
    reader.onloadend = function() {
      console.log('RESULT', reader.result)
      document.getElementById("AddFishPic").innerHTML = reader.result;
    }
    reader.readAsDataURL(file);
  }

  render() {
    return (
      <div>
        <label for="AddFishSpecies">Fish Species: </label>
        <input type="text" id="AddFishSpecies" /><br />
        <label for="AddFishPicFile">Fish Picture: </label>
        <input type="file" name="" id="AddFishPicFile" onChange={this.imageUploaded} />
        <span style={{display: 'none'}} id="AddFishPic" /><br />
        <label for="AddFishLength">Fish Length: </label>
        <input type="text" id="AddFishLength" /><br />
        <label for="AddFishWeight">Fish Weight: </label>
        <input type="text" id="AddFishWeight" /><br />
        <br />
        <Button onClick={this.handleOnClick}>Submit</Button>
        <div>{this.getTxStatus()}</div>
      </div>
    );
  }
}

export default AddFish;