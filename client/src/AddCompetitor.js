import React from "react";
import { Typography, Button } from "antd";

const { Title, Paragraph, Text } = Typography;

class AddCompetitor extends React.Component {
  state = { entryFee: null, dataKey: null, stackId: null };

  async getRosterStatus() {
    const { drizzle } = this.props;
    const contract = drizzle.contracts.FishDerby;
    const dataKey = contract.methods.roster_locked.cacheCall();
    this.setState({ dataKey });
  };

  async getEntryFee() {
    const { drizzle, drizzleState } = this.props;
    const contract = drizzle.contracts.FishDerby;
    var response;
    try {
      response = await contract.methods.entry_fee().call()
      // console.log(await response);
    }
    catch (error) {
      console.log(error);
    }
    return response;
  };

  async componentDidMount() {
    this.getRosterStatus();
    const entry_fee = await this.getEntryFee();
    this.setState({ entryFee: entry_fee });
  }

  handleOnClick = () => {
    // console.log(this.state.entryFee);
    if(this.state.entryFee === 'null') return;
    this.setValue(
      document.getElementById("AddCompetitorFirstName").value,
      document.getElementById("AddCompetitorLastName").value,
      this.state.entryFee
    );
  };

  setValue = (first_name, last_name, entry_fee) => {
    console.log(entry_fee);
    const { drizzle, drizzleState } = this.props;
    const contract = drizzle.contracts.FishDerby;
    const stackId = contract.methods.addCompetitor.cacheSend(drizzleState.accounts[0], first_name, last_name, { 
      from: drizzleState.accounts[0], 
      to: drizzle.contracts.FishDerby.address, 
      value: entry_fee 
    });
    this.setState({ stackId });
  };

  getTxStatus = () => {
    // get the transaction states from the drizzle state
    const { transactions, transactionStack } = this.props.drizzleState;

    // get the transaction hash using our saved `stackId`
    const txHash = transactionStack[this.state.stackId];

    // if transaction hash does not exist, don't display anything
    if (!txHash) return null;

    // otherwise, return the transaction status
    return `Transaction status: ${transactions[txHash] && transactions[txHash].status}`;
  };

  render() {
    const { FishDerby } = this.props.drizzleState.contracts;
    const { drizzleState } = this.props;
    const roster_status_bool = FishDerby.roster_locked[this.state.dataKey];
    if (typeof (roster_status_bool) === 'object' && roster_status_bool.value === false) {
      return (
        <div>
          <label for="AddCompetitorFirstName">First Name: </label>
          <input type="text" id="AddCompetitorFirstName" /><br />
          <label for="AddCompetitorLastName">Last Name: </label>
          <input type="text" id="AddCompetitorLastName" /><br /><br />
          <p>Entry Fee: {this.state.entryFee != null ? this.props.drizzle.web3.utils.fromWei(this.state.entryFee.toString(), 'Ether') : 0 } (Ether)</p>
          <Button onClick={this.handleOnClick}>enroll</Button>
          <div>{this.getTxStatus()}</div>
        </div>
      );
    }
    else {
      return null;
    }
  }
}

export default AddCompetitor;