import React from "react";
import FishDerby from "./contracts/FishDerby.json";
import { Button, Card } from "antd";

class CreateContract extends React.Component {
    state = { stackId: null };

    handleOnClick = () => {
        this.setContract(
            document.getElementById("CreateContractTournamentTitle").value,
            document.getElementById("CreateContractEntryFee").value,
        );
    };

    setContract = (tournament_title, funds) => {
        const { drizzle, drizzleState } = this.props;
        if(drizzle === 'undefined') alert("Error loading interface. Reload page.");
        try {
            var newContract = new drizzle.web3.eth.Contract(
                FishDerby.abi,
                {
                    from: drizzleState.accounts[0],
                    data: FishDerby.bytecode
                }

            );

            newContract.deploy({
                arguments: [tournament_title, drizzle.web3.utils.toWei(funds, 'Ether')]
            })
                .send({
                    from: drizzleState.accounts[0],
                })
                .on('transactionHash', function(hash){
                    console.log("Contract Tx Hash: " + hash);
                })
                .on('receipt', function(receipt) {
                    console.log(receipt);
                })
                .then(function (contract_instance) {
                    var contractConfig = {
                        contractName: "FishDerby",
                        web3Contract: contract_instance
                    };
                    drizzle.addContract(contractConfig);
                });
        }
        catch (error) {
            console.log(error);
        }
    };

    getTxStatus = () => {
        // get the transaction states from the drizzle state
        const { transactions, transactionStack } = this.props.drizzleState;

        // get the transaction hash using our saved `stackId`
        const txHash = transactionStack[this.state.stackId];

        // if transaction hash does not exist, don't display anything
        if (!txHash) return null;

        // otherwise, return the transaction status
        return `Transaction status: ${transactions[txHash] && transactions[txHash].status}`;
    };

    render() {
        return (
            <Card title="Create a Tournament" style={{ width: 300 }}>
                <label for="CreateContractTournamentTitle">Contract Title: </label>
                <input type="text" id="CreateContractTournamentTitle" /><br />
                <label for="CreateContractEntryFee">Entry Fee (Ether): </label>
                <input type="text" id="CreateContractEntryFee" /><br />
                <br />
                <Button onClick={this.handleOnClick}>Create</Button>
                <div>{this.getTxStatus()}</div>
            </Card>
        );
    }
}

export default CreateContract;

