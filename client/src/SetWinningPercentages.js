import React from "react";
import { Button } from "antd";
import GetWinningsPercentages from "./GetWinningsPercentages";

class SetWinningPercentages extends React.Component {
  state = { stackId: null };

  handleOnClick = () => {
    this.setValue(
      document.getElementById("SetFirstPlacePercent").value,
      document.getElementById("SetSecondPlacePercent").value,
      document.getElementById("SetThirdPlacePercent").value,
    );
  };

  setValue = (first_place_percent, second_place_percent, third_place_percent) => {
    const { drizzle, drizzleState } = this.props;
    const contract = drizzle.contracts.FishDerby;
    const stackId = contract.methods.setWinningsPercentages.cacheSend(first_place_percent, second_place_percent, third_place_percent, {
      from: drizzleState.accounts[0]
    });

    // save the `stackId` for later reference
    this.setState({ stackId });
  };

  getTxStatus = () => {
    // get the transaction states from the drizzle state
    const { transactions, transactionStack } = this.props.drizzleState;

    // get the transaction hash using our saved `stackId`
    const txHash = transactionStack[this.state.stackId];

    // if transaction hash does not exist, don't display anything
    if (!txHash) return null;

    // otherwise, return the transaction status
    return `Transaction status: ${transactions[txHash] && transactions[txHash].status}`;
  };

  render() {
    return (
      <div>
        <GetWinningsPercentages
          drizzle={this.props.drizzle}
          drizzleState={this.props.drizzleState}
        />
        <label for="SetFirstPlacePercent">First Place: </label>
        <input type="text" id="SetFirstPlacePercent" /><br />
        <label for="SetSecondPlacePercent">Second Place: </label>
        <input type="text" id="SetSecondPlacePercent" /><br />
        <label for="SetThirdPlacePercent">Third Place: </label>
        <input type="text" id="SetThirdPlacePercent" /><br />
        <br />
        <Button onClick={this.handleOnClick}>Submit</Button>
        <div>{this.getTxStatus()}</div>
      </div>
    );
  }
}

export default SetWinningPercentages;