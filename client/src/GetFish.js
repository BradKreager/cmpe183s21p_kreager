import React from "react";
import { Button } from 'antd';

class GetFish extends React.Component {
    state = { dataKey: null, tournament_title: null };

    handleOnClick = () => {
        this.getFishByIndex(
            document.getElementById("GetFishAccount").value,
            document.getElementById("GetFishIndex").value,
        );
    };

    async getFishByIndex(account, index) {
        const { drizzle, drizzleState } = this.props;
        const contract = drizzle.contracts.FishDerby;
        const dataKey = contract.methods.getFish.cacheCall(account, index, { from: drizzleState.accounts[0] });
        this.setState({ dataKey });
    };

    componentDidMount() {
    }

    render() {
        const { FishDerby } = this.props.drizzleState.contracts;
        // const title = FishDerby.getTournamentTitle[this.state.dataKey];
        const fish_data = FishDerby.getFish[this.state.dataKey];
        return (
        <div>
            <label for="GetFishAccount">Account: </label>
            <input type="text" id="GetFishAccount" /><br />
            <label for="GetFishIndex">Fish Index: </label>
            <input type="text" id="GetFishIndex" /><br />
            <Button type="primary" onClick={this.handleOnClick} >Get Fish</Button>
            <p>Fish Data: {fish_data && fish_data.value}</p>
        </div>
        // if it exists, then we display its value
        );
        // return <p>Tournament title: {this.state.tournament_title}</p>;
    }
}

export default GetFish;