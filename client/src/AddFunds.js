import React from "react";
import { Button } from "antd";

class AddFunds extends React.Component {
  state = { stackId: null };

  handleOnClick = () => {
    this.sendFunds(
        document.getElementById("AddFundsToContract").value
    );
  };

  sendFunds = (amount) => {
    const { drizzle, drizzleState } = this.props;
    const contract = drizzle.contracts.FishDerby;
    const stackId = contract.methods.deposit.cacheSend({
        from: drizzleState.accounts[0],
        to: contract.address,
        value: drizzle.web3.utils.toWei(amount, 'ether')
    });
    this.setState({ stackId });
  };

  getTxStatus = () => {
    // get the transaction states from the drizzle state
    const { transactions, transactionStack } = this.props.drizzleState;

    // get the transaction hash using our saved `stackId`
    const txHash = transactionStack[this.state.stackId];

    // if transaction hash does not exist, don't display anything
    if (!txHash) return null;

    // otherwise, return the transaction status
    return `Transaction status: ${transactions[txHash] && transactions[txHash].status}`;
  };


  render() {
    return (
      <div>
        <label for="AddFundsToContract">Add Funds (Ether): </label>
        <input type="text" id="AddFundsToContract" /><br />
        <br />
        <Button onClick={this.handleOnClick}>Add Funds</Button>
        <div>{this.getTxStatus()}</div>
      </div>
    );
  }
}

export default AddFunds;