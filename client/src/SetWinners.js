import React from "react";
import { Button } from "antd";
import GetWinners from "./GetWinners";

class SetWinners extends React.Component {
    state = { stackId: null };

    handleOnClick = () => {
        this.setValue(
        );
    };

    setValue = () => {
        const { drizzle, drizzleState } = this.props;
        const contract = drizzle.contracts.FishDerby;
        // contract.methods.setWinners().estimateGas({ from: drizzleState.accounts[0] }).then(function(e, gas) {
        //     console.log(gas);
        // });
        const stackId = contract.methods.setWinners.cacheSend({
            from: drizzleState.accounts[0]
        });

        // save the `stackId` for later reference
        this.setState({ stackId });
    };

    getTxStatus = () => {
        // get the transaction states from the drizzle state
        const { transactions, transactionStack } = this.props.drizzleState;

        // get the transaction hash using our saved `stackId`
        const txHash = transactionStack[this.state.stackId];

        // if transaction hash does not exist, don't display anything
        if (!txHash) return null;

        // otherwise, return the transaction status
        return `Transaction status: ${transactions[txHash] && transactions[txHash].status}`;
    };

    render() {
        return (
            <div>
                <GetWinners
                    drizzle={this.props.drizzle}
                    drizzleState={this.props.drizzleState}
                />
                <Button onClick={this.handleOnClick}>Set Winners</Button>
                <div>{this.getTxStatus()}</div>
            </div>
        );
    }
}

export default SetWinners;