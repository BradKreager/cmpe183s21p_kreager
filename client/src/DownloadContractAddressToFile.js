import React from "react";
import FishDerby from "./contracts/FishDerby.json";
import { Button, Card } from "antd";

class DownloadContractAddressToFile extends React.Component {
    state = { stackId: null };

    handleOnClick = () => {
        this.downloadContractAddress();
    };

    downloadContractAddress = () => {
        const { drizzle } = this.props;
        var a = document.createElement("a");
        var file = new Blob([drizzle.contractList[0].address], { type: 'text/plain' });
        a.href = URL.createObjectURL(file);
        // alert(file.value + a.href);
        a.download = document.getElementById("tournament_title").innerHTML + "_tournament_address.csv";
        a.click();
        URL.revokeObjectURL(a.href)
    }

    render() {
        return (
            <Card title="Download Tournament Address" style={{ width: 300 }}>
                <Button onClick={this.handleOnClick}>Download</Button>
            </Card>
        );
    }
}

export default DownloadContractAddressToFile;

