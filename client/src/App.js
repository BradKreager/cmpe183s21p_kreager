import React, { Component } from 'react';
import { Typography, Select, Space, Divider, Spin } from 'antd';
import { DrizzleContext } from "@drizzle/react-plugin";
import './App.css';
import GetTournamentTitle from './GetTournamentTitle';
import GetContractBalance from './GetContractBalance';
import CreateContract from './CreateContract';
import LoadContract from './LoadContract';
import LeaderBoard from './LeaderBoard';
import TournamentStatus from './TournamentStatus';
import SetCompFunctions from './SetCompFunctions';
import DownloadContractAddressToFile from './DownloadContractAddressToFile';

let Web3 = require('web3');

const { Option } = Select;
const { Title } = Typography;


class App extends Component {

  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      drizzleState: null,
      contract_loaded: false
    };
  }

  state = { loading: true, drizzleState: null };

  ethEnabled = async () => {
    if (window.ethereum) {
      await window.ethereum.send('eth_requestAccounts');
      window.web3 = new Web3(window.ethereum);
      return true;
    }
    return false;
  }

  componentDidMount() {
    const { drizzle } = this.props;
    // this.ethEnabled()

    console.log(drizzle);
    // console.log(drizzle.contractList[0].methods.contract_creator().call());
    // subscribe to changes in the store
    this.unsubscribe = drizzle.store.subscribe(() => {

      // every time the store updates, grab the state from drizzle
      const _drizzleState = drizzle.store.getState();

      // check to see if it's ready, if so, update local component state
      if (_drizzleState.drizzleStatus.initialized) {
        // this.setState({ loading: false, drizzleState: [drizzleState] });
        this.setState({ drizzleState: _drizzleState });
        this.setState({ loading: false });
      }

      if (typeof drizzle.contractList[0] === 'object') {
        this.setState({ contract_loaded: true });
      }
      window.ethereum.on('accountsChanged', function (accounts) {
        window.location.reload();
      })
    });
  }

  componentWillUnmount() {
    this.unsubscribe();
  }

  handleChange(value) {
    console.log(value);
  };

  getConnectedAccounts(drizzleState) {
    console.log(drizzleState);
    if (drizzleState.drizzleStatus.initialized) {
      return (
        Object.keys(drizzleState.accounts).map((account) => <Option value={drizzleState.accounts[account]}>{drizzleState.accounts[account]}</Option>)
      );
    }
    else {
      return (
        <Option value="none">none</Option>
      );
    }
  }

  render() {
    if (this.state.loading) {
      return (
        <div className="App">
          <br />
          <br />
          <Spin />
        </div>
      );
    }
    else if (!this.state.contract_loaded) {
      return (
        <div className="App">
          <br />
          <Title>Fish Derby Dapp</Title>
          <Divider></Divider>
          <Space direction="vertical">
            <CreateContract
              drizzle={this.props.drizzle}
              drizzleState={this.state.drizzleState}
            />
            <LoadContract
              drizzle={this.props.drizzle}
              drizzleState={this.state.drizzleState}
            />
          </Space>
        </div>
      );
    }
    else if (this.state.contract_loaded) {
      return (
        <div className="App">
          <GetTournamentTitle
            drizzle={this.props.drizzle}
            drizzleState={this.state.drizzleState}
          />
          <Space id="optCards" direction="vertical">
          <DownloadContractAddressToFile
            drizzle={this.props.drizzle}
            drizzleState={this.state.drizzleState}
          />
          <br />
          </Space>
          <GetContractBalance
            drizzle={this.props.drizzle}
            drizzleState={this.state.drizzleState}
          />
          <TournamentStatus
            drizzle={this.props.drizzle}
            drizzleState={this.state.drizzleState}
          />
          <LeaderBoard
            drizzle={this.props.drizzle}
            drizzleState={this.state.drizzleState}
          />
          <Space id="cardSpace" direction="vertical">
            <SetCompFunctions
              drizzle={this.props.drizzle}
              drizzleState={this.state.drizzleState}
            />
          </Space>
        </div>
      );
    }
  }
}

export default App;