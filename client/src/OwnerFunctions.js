import React from "react";
import { Typography, Space, Card } from "antd";
import SetTournamentTitle from './SetTournamentTitle';
import SetWinners from './SetWinners';
import EndTournament from './EndTournament';
import AddFunds from './AddFunds';
import RosterStatus from "./RosterStatus";
import SetMaxFish from "./SetMaxFish";
import SetWinningPercentages from "./SetWinningPercentages";
import SetWinnersManually from "./SetWinnersManually";

class OwnerFunctions extends React.Component {
  state = { dataKey: null };

  async getContractCreator() {
    const { drizzle } = this.props;
    const contract = drizzle.contracts.FishDerby;
    const dataKey = contract.methods.contract_creator.cacheCall();
    this.setState({ dataKey });
  };

  componentDidMount() {
    this.getContractCreator();
  }

  render() {
    const { FishDerby } = this.props.drizzleState.contracts;
    const { drizzleState } = this.props;
    const creator_address = FishDerby.contract_creator[this.state.dataKey];
    if (typeof(creator_address) === 'object' && drizzleState.accounts[0] === creator_address.value) {
      return (
        <>
            <Card title="Roster Status" style={{ width: 300 }}>
              <RosterStatus
                drizzle={this.props.drizzle}
                drizzleState={drizzleState}
              />
            </Card>
            <Card title="Declare Winners" style={{ width: 300 }}>
              <SetWinners
                drizzle={this.props.drizzle}
                drizzleState={drizzleState}
              />
            </Card>
            <Card title="End Tournament" style={{ width: 300 }}>
              <EndTournament
                drizzle={this.props.drizzle}
                drizzleState={drizzleState}
              />
            </Card>
            <Card title="Add Funds" style={{ width: 300 }}>
              <AddFunds
                drizzle={this.props.drizzle}
                drizzleState={drizzleState}
              />
            </Card>
            <Card title="Change Title" style={{ width: 300 }}>
              <SetTournamentTitle
                drizzle={this.props.drizzle}
                drizzleState={drizzleState}
              />
            </Card>
            <Card title="Set Catch Limit" style={{ width: 300 }}>
              <SetMaxFish
                drizzle={this.props.drizzle}
                drizzleState={drizzleState}
              />
            </Card>
            <Card title="Set Payout Percentages" style={{ width: 300 }}>
              <SetWinningPercentages
                drizzle={this.props.drizzle}
                drizzleState={drizzleState}
              />
            </Card>
            <Card title="Set Winners Manually" style={{ width: 300 }}>
              <SetWinnersManually
                drizzle={this.props.drizzle}
                drizzleState={drizzleState}
              />
            </Card>
            <br />
        </>
      );
    }
    else {
      return null;
    }
  }
}

export default OwnerFunctions;