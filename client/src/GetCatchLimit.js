import React from "react";
import { Typography } from "antd";

const { Text } = Typography;

class GetCatchLimit extends React.Component {
    state = { dataKey: null };

    componentDidMount() {
        const { drizzle } = this.props;
        const contract = drizzle.contracts.FishDerby;
        const dataKey = contract.methods.max_fish_per_competitor.cacheCall();
        this.setState({ dataKey });
    }

    render() {
        // get the contract state from drizzleState
        const { FishDerby } = this.props.drizzleState.contracts;
        // using the saved `dataKey`, get the variable we're interested in
        var data = FishDerby.max_fish_per_competitor[this.state.dataKey];
        // if it exists, then we display its value
        if (typeof data === 'object') {
            return (
                <>
                    <p>Current Catch Limit: {data.value} (fish)</p>
                </>
            );
        }
        else {
            return null;
        }
    }
}

export default GetCatchLimit;