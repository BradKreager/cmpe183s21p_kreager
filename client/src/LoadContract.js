import React from "react";
import FishDerby from "./contracts/FishDerby.json";
import { Button, Card } from "antd";

class LoadContract extends React.Component {
    state = { stackId: null };

    handleOnClick = () => {
        this.loadContract(
            document.getElementById("AddContractAddress").value,
        );
    };

    loadContract = (contract_address) => {
        const { drizzle, drizzleState } = this.props;
        if(contract_address === "") return alert("Enter a valid tournament address");
        try {
            var contractConfig = {
                contractName: "FishDerby",
                web3Contract: new drizzle.web3.eth.Contract(
                    FishDerby.abi,
                    contract_address,
                    {
                        from: drizzleState.accounts[0],
                        data: FishDerby.bytecode
                    }

                )
            };
            // events = [];
            drizzle.addContract(contractConfig);
        }
        catch (error) {
            console.log(error);
            alert("An incorrect address was entered");
        }
    };

    getTxStatus = () => {
        // get the transaction states from the drizzle state
        const { transactions, transactionStack } = this.props.drizzleState;

        // get the transaction hash using our saved `stackId`
        const txHash = transactionStack[this.state.stackId];

        // if transaction hash does not exist, don't display anything
        if (!txHash) return null;

        // otherwise, return the transaction status
        return `Transaction status: ${transactions[txHash] && transactions[txHash].status}`;
    };

    render() {
        return (
            <Card title="View Tournament" style={{ width: 300 }}>
                <label for="AddContractAddress">Contract Address: </label>
                <input type="text" id="AddContractAddress" /><br />
                <br />
                <Button onClick={this.handleOnClick}>View</Button>
                <div>{this.getTxStatus()}</div>
            </Card>
        );
    }
}

export default LoadContract;

