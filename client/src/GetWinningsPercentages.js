import React from "react";
import { Typography } from "antd";

const { Text } = Typography;

class GetWinningsPercentages extends React.Component {
    state = { dataKey: null };

    componentDidMount() {
        const { drizzle } = this.props;
        const contract = drizzle.contracts.FishDerby;
        const dataKey = contract.methods.getWinningsPercentages.cacheCall();
        this.setState({ dataKey });
    }

    render() {
        const { FishDerby } = this.props.drizzleState.contracts;
        var data = FishDerby.getWinningsPercentages[this.state.dataKey];

        if (typeof data === 'object' && data.value) {
            return (
                <>
                    <p>Current Percentages: </p>
                    <p>First Place: {data.value[0]}% (Ether)</p>
                    <p>Second Place: {data.value[1]}% (Ether)</p>
                    <p>Third Place: {data.value[2]}% (Ether)</p>
                </>
            );
        }
        else {
            return null;
        }
    }
}

export default GetWinningsPercentages;