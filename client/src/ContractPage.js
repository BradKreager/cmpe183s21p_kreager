import React, { Component } from 'react';
import { DrizzleContext } from "@drizzle/react-plugin";
import './App.css';
import GetTournamentTitle from './GetTournamentTitle';
import GetCompetitorStats from './GetCompetitorStats';
import GetRoster from './GetRoster';
import AddRoster from './AddRoster';
import AddCompetitor from './AddCompetitor';
import SetTournamentTitle from './SetTournamentTitle';
import AddFish from './AddFish';
import GetFish from './GetFish';
import AddFunds from './AddFunds';
import GetContractBalance from './GetContractBalance';
import EndTournament from './EndTournament';
import CreateContract from './CreateContract';
import LoadContract from './LoadContract';

let Web3 = require('web3');

class App extends Component {

    constructor(props) {
        super(props);
        this.state = {
            loading: true,
            drizzleState: null,
            contract_loaded: false
        };
    }

    state = { loading: true, drizzleState: null };

    ethEnabled = async () => {
        if (window.ethereum) {
            await window.ethereum.send('eth_requestAccounts');
            window.web3 = new Web3(window.ethereum);
            return true;
        }
        return false;
    }

    async getCompetitor(account) {
        const { drizzle } = this.props;
        const contract = drizzle.contracts.FishDerby;
        const dataKey = contract.methods.getCompetitor.cacheCall(account);
        this.setState({ dataKey });
    };

    componentDidMount() {
        const { drizzle, drizzleState } = this.props;

        drizzle.contractList[0].methods.contract_creator().call().then(function (res) { console.log(res); });
        // this.getCompetitor(drizzleState.accounts[0]);
    }

    componentWillUnmount() {
    }

    render() {
        if (this.state.loading) {
            return "Loading Drizzle...";
        }
        else if (!this.state.contract_loaded) {
            return (null
            );

        }
        else if (this.state.contract_loaded) {
            return (
                <div className="App">
                    <GetTournamentTitle
                        drizzle={this.props.drizzle}
                        drizzleState={this.state.drizzleState}
                    />
                    <AddFunds
                        drizzle={this.props.drizzle}
                        drizzleState={this.state.drizzleState}
                    />
                    <GetContractBalance
                        drizzle={this.props.drizzle}
                        drizzleState={this.state.drizzleState}
                    />
                    <GetCompetitorStats
                        drizzle={this.props.drizzle}
                        drizzleState={this.state.drizzleState}
                    />
                    <GetRoster
                        drizzle={this.props.drizzle}
                        drizzleState={this.state.drizzleState}
                    />
                    <SetTournamentTitle
                        drizzle={this.props.drizzle}
                        drizzleState={this.state.drizzleState}
                    />
                    <AddCompetitor
                        drizzle={this.props.drizzle}
                        drizzleState={this.state.drizzleState}
                    />
                    <AddFish
                        drizzle={this.props.drizzle}
                        drizzleState={this.state.drizzleState}
                    />
                    <GetFish
                        drizzle={this.props.drizzle}
                        drizzleState={this.state.drizzleState}
                    />
                    <EndTournament
                        drizzle={this.props.drizzle}
                        drizzleState={this.state.drizzleState}
                    />
                </div>
            );
        }
    }
}

export default App;