import React from "react";
import { Typography } from "antd";

const { Text } = Typography;

class GetWinners extends React.Component {
    state = { dataKey: null, tournament_title: null };

    async getWinners() {
        const { drizzle } = this.props;
        const contract = drizzle.contracts.FishDerby;
        const dataKey = contract.methods.getWinners.cacheCall();
        this.setState({ dataKey });
    };

    componentDidMount() {
        const { drizzle } = this.props;
        this.getWinners();
    }

    displayWinners(accounts_object) {
        if (accounts_object === null || typeof accounts_object === 'undefined') return null;
        const accounts = accounts_object.value;
        const res = [];
        for (const account of accounts) {
            res.push(<Text
                style={{ width: 250 }}
                ellipsis={{ tooltip: account }}
            >
                {account}
            </Text>);
        }
        return res;
    }

    render() {
        // get the contract state from drizzleState
        const { FishDerby } = this.props.drizzleState.contracts;

        // using the saved `dataKey`, get the variable we're interested in
        var winners = FishDerby.getWinners[this.state.dataKey];

        // if it exists, then we display its value
        return (
            <>
                <p>Tournament winners:</p>
                {this.displayWinners(winners)}
            </>
        );
    }
}

export default GetWinners;