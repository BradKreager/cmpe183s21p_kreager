import React from "react";
import { List, Typography, Button, Skeleton } from 'antd';

const { Text } = Typography;

class LeaderBoard extends React.Component {
  state = {
    initLoading: true,
    loading: false,
    data: [],
    list: [],
  };

  componentDidMount() {
    const { drizzle, drizzleState } = this.props;
    const contract = drizzle.contracts.FishDerby;
    this.timer = setInterval(async () => {
      const roster = await contract.methods.getRoster().call()
      // console.log(roster);
      const board = await this.getLeaderBoard(roster);
      console.log(board);
      this.setState({ data: board, initLoading: false });
    }, 3000);
  }

  componentDidUpdate() {

  }

  componentWillUnmount() {
    clearInterval(this.timer);
  }

  async getCompetitors(roster) {
    const { drizzle, drizzleState } = this.props;
    const contract = drizzle.contracts.FishDerby;
    var competitor_list = [];
    // return new Promise(resolve => {
    if (typeof roster === 'object') {
      for (const account of roster) {
        try {
          const response = await contract.methods.getCompetitor(account).call()
          competitor_list.push(await response);
          // console.log(await response);
        }
        catch (error) {
          console.log(error);
        }
      }
      // console.log(competitor_list);
    }
    return competitor_list;
    // resolve(competitor_list);
    // });
  };

  async getLeaderBoard(roster) {
    var competitor_list = await this.getCompetitors(roster);
    return new Promise(resolve => {
      try {
        competitor_list.sort((a, b) => { return ((b.total_inventory_weight) - (a.total_inventory_weight))});
        resolve(competitor_list);
      }
      catch (error) {
        console.log(error);
      }
    });
  };

  getBiggestFish(fish_inventory) {
    if (Array.isArray(fish_inventory) && fish_inventory.length === 0) {
      return 0;
    }
    const fish = fish_inventory.reduce((a, b) => a.weight > b.weight ? a : b);
    return fish.weight;
  }

  getIndexOfBiggestFish(fish_inventory) {
    if (Array.isArray(fish_inventory) && fish_inventory.length === 0) {
      return 0;
    }
    const biggest_fish_weight = this.getBiggestFish(fish_inventory);
    for (let i = 0; i < fish_inventory.length; i++) {
      if (fish_inventory[i].weight === biggest_fish_weight) return i;
    }
  };

  getSpeciesOfBiggestFish(fish_inventory) {
    if (Array.isArray(fish_inventory) && fish_inventory.length === 0) {
      return "No Fish Yet";
    }
    return fish_inventory[this.getIndexOfBiggestFish(fish_inventory)].fish_species
  }

  render() {
    return (
      <>
        <List
          size="large"
          header={<Text strong>Leader Board</Text>}
          bordered
          dataSource={this.state.data}
          renderItem={item => (
            <List.Item>
              {item.first_name} {item.last_name} - {item.total_inventory_weight} kgs
              {/* {item.first_name} {item.last_name} - {this.getSpeciesOfBiggestFish(item.fish_inventory)} - {this.getBiggestFish(item.fish_inventory)} kgs */}
            </List.Item>
          )
          }
        />
      </>
    );
  };
}

export default LeaderBoard;
