import React from "react";
import { Typography, Space, Card, List } from "antd";
import AddCompetitor from './AddCompetitor';
import AddFish from './AddFish';

const { Title, Paragraph, Text } = Typography;

class GetCompetitorStats extends React.Component {
  state = { dataKey: null, contract_creator_key: null };

  async getCompetitor(account) {
    const { drizzle, drizzleState } = this.props;
    const contract = drizzle.contracts.FishDerby;
    const dataKey = contract.methods.getCompetitor.cacheCall(account);
    const contract_creator_key = contract.methods.checkCompetitorIsEnrolled.cacheCall(account);
    this.setState({ dataKey });
    this.setState({ contract_creator_key });
  };

  componentDidMount() {
    const { drizzleState } = this.props;
    this.getCompetitor(drizzleState.accounts[0]);
  }

  displayFishInventory(inv) {
    return (
      // Object.keys(inv).map((stats) => console.log(inv[stats]._length)) 
      Object.keys(inv).map((stats) => (
        <>
          <List.Item>
            <Text strong>Fish Number: {parseInt(stats, 10) + 1}</Text>
          </List.Item>
          <List.Item>Fish Species: {inv[stats].fish_species}</List.Item>
          <List.Item>Fish Length: {inv[stats]._length}</List.Item>
          <List.Item>Fish Weight: {inv[stats].weight}</List.Item>
        </>
      )
      )
    );
  }

  render() {
    const { FishDerby } = this.props.drizzleState.contracts;
    const stats = FishDerby.getCompetitor[this.state.dataKey];
    const is_a_competitor = FishDerby.checkCompetitorIsEnrolled[this.state.contract_creator_key];

    if (typeof is_a_competitor === 'object' && typeof stats === 'object' && is_a_competitor.value === true) {
      return (
        <>
          <Card title="Competitor Info" style={{ width: 300 }}>
            <p>First Name: {stats.value[0]}</p>
            <p>Last Name: {stats.value[1]}</p>
            <List
              size="large"
              header={<Text strong>Fish Inventory</Text>}
              bordered>
              {this.displayFishInventory(stats.value.fish_inventory)}
            </List>
          </Card>
          <br />
          <Card title="Add Fish" style={{ width: 300 }}>
            <AddFish
              drizzle={this.props.drizzle}
              drizzleState={this.props.drizzleState}
            />
          </Card>
        </>
      );
    }
    else {
      return (
        <>
          <Space direction="vertical">
            <Card title="Competitor Info" style={{ width: 300 }}>
              <p>You are not enrolled in this competition</p>
              <AddCompetitor
                drizzle={this.props.drizzle}
                drizzleState={this.props.drizzleState}
              />
            </Card>
          </Space>
        </>
      );
    }
  }
}

export default GetCompetitorStats;