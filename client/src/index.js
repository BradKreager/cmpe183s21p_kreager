import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
// import drizzle functions and contract artifact
import { Drizzle, generateStore, EventActions } from "@drizzle/store";
import { DrizzleContext } from "@drizzle/react-plugin";
import FishDerby from "./contracts/FishDerby.json";

const contractEventNotifier = store => next => action => {
  if (action.type === EventActions.EVENT_FIRED) {
    const contract = action.name
    const contractEvent = action.event.event
    const contractMessage = action.event.returnValues._message
    const display = `${contract}(${contractEvent}): ${contractMessage}`
 
    // interact with your service
    console.log('Contract event fired', display)
  }
  return next(action)
}

const appMiddlewares = [ contractEventNotifier ]

// let drizzle know what contracts we want and how to access our test blockchain
const drizzleOptions = {
  web3: {
    fallback: {
      type: "http",
      url: "http://127.0.0.1:9545",
    },
  },
};

// setup drizzle
const drizzleStore = generateStore({
  drizzleOptions,
  appMiddlewares,
  disableReduxDevTools: false  // enable ReduxDevTools!
 });

const drizzle = new Drizzle(drizzleOptions);

ReactDOM.render(
  <DrizzleContext.Provider drizzle={drizzle}>
    <App drizzle={drizzle} />
  </DrizzleContext.Provider>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
// reportWebVitals();
