import React from "react";
import { Divider, Typography } from "antd";

const { Title } = Typography;

class GetTournamentTitle extends React.Component {
  state = { dataKey: null, tournament_title: null };

  async getTitle() {
    const { drizzle } = this.props;
    const contract = drizzle.contracts.FishDerby;
    const dataKey = contract.methods.tournament_title.cacheCall();
    this.setState({ dataKey });
  };

  componentDidMount() {
    const { drizzle } = this.props;
    this.getTitle();
  }

  render() {
    const { FishDerby } = this.props.drizzleState.contracts;
    const title = FishDerby.tournament_title[this.state.dataKey];

    if(typeof title === 'object') {
      return (
        <>
          <br />
          <Title id="tournament_title">{title.value}</Title>
          <Divider></Divider>
        </>
      );
    }
    else {
      return <Title id="tournament_title">No Title</Title>;
    }
  }
}

export default GetTournamentTitle;