import React from "react";
import { Button } from "antd";

class SetTournamentTitle extends React.Component {
  state = { stackId: null };

  handleOnClick = () => {
    this.setValue(
      document.getElementById("SetTournamentTitle").value,
    );
  };

  setValue = value => {
    const { drizzle, drizzleState } = this.props;
    const contract = drizzle.contracts.FishDerby;
    const stackId = contract.methods.setTournamentTitle.cacheSend(value, {
      from: drizzleState.accounts[0]
    });

    // save the `stackId` for later reference
    this.setState({ stackId });
  };

  getTxStatus = () => {
    // get the transaction states from the drizzle state
    const { transactions, transactionStack } = this.props.drizzleState;

    // get the transaction hash using our saved `stackId`
    const txHash = transactionStack[this.state.stackId];

    // if transaction hash does not exist, don't display anything
    if (!txHash) return null;

    // otherwise, return the transaction status
    return `Transaction status: ${transactions[txHash] && transactions[txHash].status}`;
  };

  render() {
    return (
      <div>
        <label for="SetTournamentTitle">Title: </label>
        <input type="text" id="SetTournamentTitle" /><br />
        <br />
        <Button onClick={this.handleOnClick}>Submit</Button>
        <div>{this.getTxStatus()}</div>
      </div>
    );
  }
}

export default SetTournamentTitle;