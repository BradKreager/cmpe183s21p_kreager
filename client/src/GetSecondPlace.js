import React from "react";
import { Typography } from "antd";

const { Text } = Typography;

class GetSecondPlace extends React.Component {
    state = { dataKey: null };

    componentDidMount() {
        const { drizzle } = this.props;
        const contract = drizzle.contracts.FishDerby;
        const dataKey = contract.methods.second_place.cacheCall();
        this.setState({ dataKey });
    }

    render() {
        // get the contract state from drizzleState
        const { FishDerby } = this.props.drizzleState.contracts;
        // using the saved `dataKey`, get the variable we're interested in
        var data = FishDerby.second_place[this.state.dataKey];
        // if it exists, then we display its value
        if (typeof data === 'object') {
            return (
                <>
                    <Text
                        style={{ width: 250 }}
                        ellipsis={{ tooltip: data.value }}
                    >
                        Second Place: {data.value}
                    </Text>
                </>
            );
        }
        else {
            return null;
        }
    }
}

export default GetSecondPlace;