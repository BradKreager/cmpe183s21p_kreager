import React from "react";

class GetContractBalance extends React.Component {
    state = { dataKey: null, contractBalance: 0 };

    async getContractBalance() {
        const { drizzle } = this.props;
        const contract = drizzle.contracts.FishDerby;
        const dataKey = contract.methods.getBalance.cacheCall();
        this.setState({ dataKey });
    };

    componentDidMount() {
        this.getContractBalance();
    }

    render() {
        const { FishDerby } = this.props.drizzleState.contracts;
        const { drizzle } = this.props;
        var balance = 0;
        const res = FishDerby.getBalance[this.state.dataKey];
        if (typeof (res) === 'object' && res.value !== null) {
            balance = drizzle.web3.utils.fromWei(res.value);
        }
        return <p>Tournament balance: {balance} (Ether)</p>;
    }
}

export default GetContractBalance;