import React from "react";
import { Typography } from "antd";
import OwnerFunctions from './OwnerFunctions';
import GetCompetitorStats from './GetCompetitorStats';

const { Title } = Typography;

class SetCompFunctions extends React.Component {
    state = { dataKey: null, tournament_title: null };

    async getTitle() {
        const { drizzle } = this.props;
        const contract = drizzle.contracts.FishDerby;
        const dataKey = contract.methods.contract_creator.cacheCall();
        this.setState({ dataKey });
    };

    componentDidMount() {
        const { drizzle } = this.props;
        this.getTitle();
    }

    render() {
        const { FishDerby } = this.props.drizzleState.contracts;
        const contract_creator_address = FishDerby.contract_creator[this.state.dataKey];

        if (typeof contract_creator_address === 'object' && contract_creator_address.value === this.props.drizzleState.accounts[0]) {
            return (
                <OwnerFunctions
                    drizzle={this.props.drizzle}
                    drizzleState={this.props.drizzleState}
                />
            );
        }
        else if (typeof contract_creator_address === 'object' && contract_creator_address.value !== this.props.drizzleState.accounts[0]) {
            return (
                <GetCompetitorStats
                    drizzle={this.props.drizzle}
                    drizzleState={this.props.drizzleState}
                />
            );
        }
        else {
            return null;
        }
    }
}

export default SetCompFunctions;