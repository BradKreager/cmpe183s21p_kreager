import React from "react";
import { Typography } from "antd";

const { Title } = Typography;

class TournamentStatus extends React.Component {
  state = { dataKey: null, tournament_title: null };

  async getTitle() {
    const { drizzle } = this.props;
    const contract = drizzle.contracts.FishDerby;
    const dataKey = contract.methods.tournament_locked.cacheCall();
    // const dataKey = contract.methods.getTournamentTitle.cacheCall();
    this.setState({ dataKey });
  };

  componentDidMount() {
    const { drizzle } = this.props;
    this.getTitle();
  }

  render() {
    const { FishDerby } = this.props.drizzleState.contracts;
    const title = FishDerby.tournament_locked[this.state.dataKey];

    if(typeof title === 'object' && title.value === false) {
        return <p>Tournament in progress</p>;
    }
    else if(typeof title === 'object' && title.value === true) {
        return <p>Tournament is over</p>;
    }
    else {
        return null;
    }
  }
}

export default TournamentStatus;