# Fish Derby Dapp

This is the project submission for CMPE 183 Blockchain and Crypto - Bradley Kreager - Spring 2021

## Running the application

### Installing ganache-cli

run the command `npm install -g ganache-cli` from a terminal. Node.js must be installed on the system before running
the command.

### Running a ganache test chain

run the command `ganache-cli` from a terminal, it will start a test chain at 127.0.0.1 on port 8545.

### Starting the React Front end

In a terminal from within the client directory run `npm start`


### Connecting MetaMask

Under network choose the localhost network on port 8545. 
Connect to the test chain's wallet using the mnemonic provided by `ganache-cli`

## Creating a Tournament

On the front page, under title "Create a Tournament", enter a title for the tournament and an entry
fee in Ether. Click the button labeled, "Create", to submit the transaction. Once the transaction
is complete the contract page will be loaded showing the Tournament Creator Functions. After 
creation of the contract, owner functions can be reached by entering the tournament address
into the View Tournament field with the tournament creator as the active account loaded into Metamask.

## Loading a Tournament

On the front page, under title "View Tournament", enter a valid Fish Derby contract address.
Click the button labeled, "View", to go to the contract page.

## Tournament Creator Functions

### Locking the Roster

Under the title "Roster Status" click the button labeled "Lock Contract"

### Unlocking the Roster

Under the title "Roster Status" click the button labeled "Unlock Contract"

### Declaring the Winners

Under the title "Declare Winners" click the button labeled "Set Winners". Declaring the winners will lock the competition.

### Ending the Tournament

Under the title "End Tournament" click the button labeled "End Tournament". 
Ending the tournament will disperse the payouts to the winners and can only be called after declaring
the winners.

### Adding Funds to the Tournament Payout

Under the title "Add Funds" enter an amount of Ether to send to the tournament contract
and click "Add Funds". Funds can only be retrieved by destroying the contract and will
be sent back to the tournament creator.

### Changing the Tournament Title

Under the title "Change Title" enter a new title into the field and click "submit".

### Setting the Catch Limit

Under the title "Set Catch Limit" enter a new catch limit for amount of fish each competitor can have
in their fish inventory for valid points in the competition. Click "submit" to send the transaction.

### Setting the Payout Percentages

Under the title "Set Payout Percentages" enter new payout percentages for first, second, and third
place payouts. Valid numbers are between 0 - 100. Click "submit" to send the transaction.

### Setting the Winners Manually

Under the title "Set Winners Manually", enter the public key of a competitor account that exists
in the tournament roster. If this is used after the winners are declared, it will swap accounts.
For example, in a tie situation where it is desired to switch the third place account with the
second place account, only one transaction is necessary. Setting the third place account to 
second place will swap the third and second place accounts. Setting the second place account to
third place will perform the same action, both third and second place accounts will be swapped.

## Competitor Functions

### Signing up for the Tournament

Load the contract address using an account that is not the tournament creator account. Under the title
"Competitor Info", enter the competitors first name and last name, then click the button labeled
"enroll"

### Adding a Fish to the Fish Inventory

Using an account that has been enrolled into the competition load the contract address from the front
page. Under the title "Add Fish", enter the species of fish, the fish length, and the fish weight.
Click the button labeled "submit" to enter the fish into the competition. NOTE: fish picture is
a development feature that is not working and no file should be loaded using the file selector.

## Unit Tests

The Truffle Suite framework must be installed to run the unit tests

Intall the truffle framework with `npm install truffle -g`

Start a truffle develop test chain in a terminal with `truffle develop`

Once the development console has loaded, type `test`
